module.exports = {
  entry: {
    index: ['babel-polyfill', './src/index.js']
  },
  output: {
    filename: 'bundle.js',
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/env', '@babel/react']
          },
        },
      },
      {
        test: /.(css)$/,
        use: ['style-loader', 'css-loader'],
      },
    ]
  }
}
