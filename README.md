# two auto api testing

Tools untuk menguji API 2auto

## Penggunaan

pastikan server 'two auto backend' sedang berjalan di "localhost:3000", kemudian jalankan

```sh
$ npm i
$ npm start
```

setelah itu, silahkan buka "localhost:5000"

## Editing

jika ingin mengedit komponen, silahkan jalankan

```sh
$ npm run dev
```

untuk mengupdate file ```dist/bundle.js``` setiap terjadi perubahan
