import React from 'react';
import './ApiGroup.css';

class ApiGroup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showSection: true,
    };
    this.toggleSection = this.toggleSection.bind(this)
  }

  toggleSection() {
    this.setState({showSection: !this.state.showSection});
  }

  render() {
    const baseUrl = this.props.base;
    const bearerToken = this.props.token;

    return (
      <div className="ApiGroup">
        <div className="ApiGroup-header" onClick={ this.toggleSection }>
          <span className="indicator">{ this.state.showSection ? 'v' : '<' }</span>
          <div className="title">{ this.props.title }</div>
          <div className="description">{ this.props.desc }</div>
        </div>
        <div className={ `ApiGroup-section ${ this.state.showSection ? '' : 'hidden' }` }>
          { React.Children.map(this.props.children, child => {
            return React.cloneElement(child, { base: baseUrl, token: bearerToken })
          }) }
        </div>
      </div>
    )
  }
}

export default ApiGroup;
