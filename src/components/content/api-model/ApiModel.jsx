import React from 'react';
import './ApiModel.css';

class ApiModel extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      showBody: false,
    }
    this.toggleBody = this.toggleBody.bind(this);
  }

  toggleBody() {
    this.setState({showBody: !this.state.showBody});
  }

  render() {
    return (
      <div className="ApiModel">
        <div className="ApiModel-title" onClick={ this.toggleBody }>
          <span className="indicator">{ this.state.showBody ? 'v' : '<' }</span>
          { this.props.title }
        </div>
        <div className={ `ApiModel-body ${ this.state.showBody ? '' : 'hidden' }` }>
          { JSON.stringify(this.props.field) }
        </div>
      </div>
    )
  }
}

export default ApiModel;
