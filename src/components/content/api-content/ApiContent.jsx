import React from 'react';
import './ApiContent.css';
import Axios from 'axios';

class ApiContent extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      showBody: false,
      output: null,
    }
    this.objToInputArray = this.objToInputArray.bind(this);
    this.toggleBody = this.toggleBody.bind(this);
    this.request = this.request.bind(this);
  }

  objToInputArray(obj, type = 'body') {
    var array = [];
    for (const key in obj) {
      array.push({key, value: obj[key]});
    }
    switch (type) {
      case 'body':
        this.bodyForm = {};
        break;
      case 'query':
        this.queryForm = {};
        break;
      case 'params':
        this.paramsForm = {};
        break;
    }
    return array.map(obj => {
      const key = obj.key;
      return (
        <div key={ key }>
          <label htmlFor={ key }></label>
          <input type="text" placeholder={ key } name={ key } ref={ ref => {
            switch (type) {
              case 'body':
                this.bodyForm[key] = ref;
                break;
              case 'query':
                this.queryForm[key] = ref;
                break;
              case 'params':
                this.paramsForm[key] = ref;
                break;
            }            
          } }/>
        </div>
      )
    });
  }

  toggleBody() {
    this.setState({showBody: !this.state.showBody});
  }

  getRefsValue(refs) {
    const obj = {};
    for(const key in refs) {
      obj[key] = refs[key].value;
    }
    return obj;
  }

  async request() {
    let path = this.props.path;
    if (this.props.params) {
      const paramsData = this.getRefsValue(this.paramsForm);
      for(const key in paramsData) {
        path = path.replace(`:${ key }`, paramsData[key] || '0');
      }
    }
    const options = {
      method: this.props.method,
      baseURL: this.props.base,
      url: path,
    };
    if (this.props.token) {
      options.headers = {
        Authorization: this.props.token,
      }
    }
    if (this.props.body) {
      const bodyData = this.getRefsValue(this.bodyForm);
      options.data = bodyData;
    }
    if (this.props.query) {
      const queryData = this.getRefsValue(this.queryForm);
      options.params = queryData;
    }
    try {
      const respond = await Axios(options);
      this.setState({output: respond.data})  
    } catch (err) {
      this.setState({output: err.response.data});
    }
  }

  render() {
    return (
      <div className="ApiContent">
        <div className="ApiContent-header" onClick={ this.toggleBody }>
          <div className="ApiContent-method">{ this.props.method }</div>
          <div className="ApiContent-path">{ this.props.path }</div>
          <div className="ApiContent-desc">{ this.props.desc }</div>
          <div className={ this.props.protected ? 'protected' : 'public' }>public</div>  
        </div>
        <div className={ `ApiContent-body ${ this.state.showBody ? '' : 'hidden' }` }>
          <div className="input">
            <div className="params">
              Params : { this.objToInputArray(this.props.params, 'params') }
            </div>
            <div className="query">
              Query : { this.objToInputArray(this.props.query, 'query') }
            </div>
            <div className="body">
              Body : { this.objToInputArray(this.props.body) }
            </div>
            <button onClick={ this.request }>Try it</button>
          </div>
          <div className="output">
            output : <br/>
            { JSON.stringify(this.state.output) }
          </div>
        </div>
      </div>
    )
  }
}

export default ApiContent;
