import React from 'react';
import './Content.css';
import ApiContent from './api-content/ApiContent.jsx';
import ApiGroup from './api-group/ApiGroup.jsx';
import ApiModel from './api-model/ApiModel.jsx';

class Content extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      token: null,
    };
    this.authenticate = this.authenticate.bind(this);
  }

  authenticate() {
    var token = prompt('Masukkan Token');
    this.setState({token});
  }

  render() {
    const token = this.state.token;
    const bearerToken = token ? `Bearer ${ token }` : null;
    const baseUrl = this.props.base;

    return (
      <div className="Content">
        <div className="Content-title">
          <h3>{ this.props.title }</h3>
          <code>BaseUrl : { this.props.base }</code>
          <p>{ this.props.desc }</p>
        </div>

        <div className="Content-config">
          <div>
            <label htmlFor="schema">Schema</label>
            <br/>
            <select name="schema">
              <option value="http">http</option>
            </select>
          </div>
          <div>
            <button onClick={ this.authenticate }>Authorize</button>
          </div>
        </div>

        <hr/>

        <div className="Content-section">
          <ApiGroup title="Workshop" desc="this is Workshop" base={ baseUrl } token={ bearerToken }>
            <ApiContent method="post" path="/workshop/nearest" desc="Testing nearest"
              body={{ lat: "number", lng: "number", radius: "number" }} />
          </ApiGroup>
          <ApiGroup title="Auth" desc="this is auth" base={ baseUrl } token={ bearerToken }>
            <ApiContent method="get"  path="/"                desc="Testing page" />
            <ApiContent method="post" path="/login"           desc="Getting Token"
              body={loginForm} />
            <ApiContent method="post" path="/register"        desc="Registering new User"
              body={loginForm} />
            <ApiContent method="post" path="/reset"           desc="Getting token to reset password"
              body={resetForm} />
            <ApiContent method="post" path="/reset-email"     desc="Send email to reset"
              body={resetForm} />
            <ApiContent method="post" path="/reset-password"  desc="Reset using token" 
              query={resetToken} body={resetPasswordForm} />
          </ApiGroup>
          <hr/>
          <ApiGroup title="User" desc="api for user model" base={ baseUrl } token={ bearerToken }>
            <ApiContent method="get"    path="/user"      desc="Getting users"  protected={true} />
            <ApiContent method="post"   path="/user"      desc="Add new User"   protected={true} />
            <ApiContent method="get"    path="/user/:id"  desc="Getting a User" protected={true}
              params={idParams} />
            <ApiContent method="put"    path="/user/:id"  desc="Update User"    protected={true}
              params={idParams} />
            <ApiContent method="delete" path="/user/:id"  desc="Delete User"    protected={true}
              params={idParams} />
          </ApiGroup>
          <hr/>
          <ApiGroup title="Partner" desc="api for partner model" base={ baseUrl } token={ bearerToken }>
            <ApiContent method="get"    path="/partner"      desc="Getting partners"  protected={true} />
            <ApiContent method="post"   path="/partner"      desc="Add new partner"   protected={true} />
            <ApiContent method="get"    path="/partner/:id"  desc="Getting a partner" protected={true}
              params={idParams} />
            <ApiContent method="put"    path="/partner/:id"  desc="Update partner"    protected={true}
              params={idParams} />
            <ApiContent method="delete" path="/partner/:id"  desc="Delete partner"    protected={true}
              params={idParams} />
            <ApiContent method="post"   path="/partner/reset"           desc="Getting token to reset password"
              body={resetForm} />
            <ApiContent method="post"   path="/partner/reset-email"     desc="Send email to reset"
              body={resetForm} />
            <ApiContent method="post"   path="/partner/reset-password"  desc="Reset using token" 
              query={resetToken} body={resetPasswordForm} />
          </ApiGroup>
          <hr/>
        </div>

        <div className="Content-model">
          <div className="title">Model</div>
          <div className="models">
            <ApiModel title="User" field={userModel} />
          </div>
        </div>
      </div>
    )
  }
}



const loginForm = {
  email: 'string',
  password: 'string',
};

const resetForm = {
  email: 'string',
};

const resetPasswordForm = {
  password: 'string',
}

const idParams = {
  id: 'integer',
}

const resetToken = {
  token: 'string',
}



const userModel = {
  id: 'integer',
  email: 'string',
  password: 'string',
  createdAt: 'date',
  updatedAt: 'date',
  deletedAt: 'date',
};

export default Content;
