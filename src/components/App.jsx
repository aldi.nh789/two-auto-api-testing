import React from 'react';
import './App.css';
import Content from './content/Content.jsx';

class App extends React.Component {

  render() {
    return (
      <div className="App">
        <div className="App-header">2auto Backend Api Testing</div>

        <Content
          title="2auto API Testing"
          base="http://localhost:3000"
          desc="this is tool for testing 2auto apis"/>
      </div>
    )
  }
}

export default App;
