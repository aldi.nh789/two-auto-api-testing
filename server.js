const http = require('http');
const serveHandler = require('serve-handler');

const server = http.createServer((req, res) => {
  return serveHandler(req, res, {
    public: 'dist',
  })
});

server.listen(5000, _ => {
  console.log(`Listening on Port ${ server.address().port }`)
});
